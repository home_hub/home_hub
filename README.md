Home_Hub
========

Main program design in Qt, to work as Hub for sensors. 

To get some more accurate information pleas refere to project Wiki. 

## Main assumptions

This project is developed as postgraduate project in "Programming with elements of embedded systems" at the West Pomeranian University of Technology. 

Project was devided in three submodules: 

[Operating system][os] 
[Main application][home_hub]
[Temperatur node][temp_node] 

Functions
=========

#### 1. All Application functions:

#### Main Hub

* receiving and mainteining sensor data send over wi-fi, Bluetooth or other medium.
* Showing data on screen 
* Connecting to Wi-Fi
* Creating "rooms" as data groups

#### Sensor Nodes

* Collecting data from buildin sensors
* Creating serialize frame with collected data
* Send data to hub.

#### 2. Hardware requirements:

##### Main Hub

* Raspberry Pi Zero
* Accesroies to Raspberry Pi: SD Card(16GB), Power Supply  
* HDMI cable
* Touch Screen LCD IPS 7", 1024x600px DPI
* Radio Module nRF24L01+ 2,4GHz - transceiver THT 

##### Sesnsor Node

* AVR Attinny 85 microprocessor
* BME280 - humidity, temperature and pressure sensor 110kPa I2C / SPI - 3.3V
* Radio Module nRF24L01+ 2,4GHz - transceiver THT

#### 3. Required development tools: 

* Raspberian or custom kernel design for Raspberry Pi and Qt programming
* Eagle for design circuits
* Visual Studio Code
* Qt creator

Author
======

Grzegorz Rezmer - Junior Software Engineer at Tieto Poland. 

Supervisor
=========

dr inż. Radosław Maciaszczyk - senior lecturer 
West Pomeranian University of Technology

[os]: https://gitlab.com/home_hub/os.git
[home_hub]: https://gitlab.com/home_hub/home_hub.git
[temp_node]: https://gitlab.com/home_hub/temp_node.git