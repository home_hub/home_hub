QT += quick qml quickcontrols2 core concurrent
CONFIG += qtquickcompiler disable-desktop qtvirtualkeyboard.pro c++17

static {
    QT += svg
    QTPLUGIN += qtvirtualkeyboardplugin
}

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    bin/C++/networkModel.cpp \
    bin/C++/wifilist.cpp \
    bin/C++/radiothreadmenager.cpp \
    bin/C++/nrfradio.cpp \
    bin/C++/sensordataparser.cpp \
    bin/C++/temandhumdevice.cpp \
    bin/C++/Data_Kontainers/temphumsensordatakontainer.cpp \
    bin/C++/devicecontroller.cpp \
    bin/C++/DeviceControllers/temdevicemodel.cpp \
    bin/C++/DeviceControllers/builder.cpp

RESOURCES += \
    qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.

target.path = /home/pi/$${TARGET}
INSTALLS += target

disable-desktop|android-embedded|!isEmpty(CROSS_COMPILE)|qnx {
    DEFINES += MAIN_QML=\\\"basic-b2qt.qml\\\"
} else {
    DEFINES += MAIN_QML=\\\"Basic.qml\\\"
}

HEADERS += \
    bin/C++/networkModel.h \
    bin/C++/wifilist.h \
    bin/C++/radiothreadmenager.h \
    bin/C++/nrfradio.h \
    bin/C++/sensordataparser.h \
    bin/C++/devicecontroller.h \
    bin/C++/deviceinterface.h \
    bin/C++/temandhumdevice.h \
    bin/C++/Data_Kontainers/datakonteinerinterface.h \
    bin/C++/Data_Kontainers/temphumsensordatakontainer.h \
    bin/C++/DeviceControllers/devicecontrollerinterface.h \
    bin/C++/DeviceControllers/devicefactory.h \
    bin/C++/DeviceControllers/temdevicemodel.h \
    bin/C++/DeviceControllers/builder.h

DISTFILES += \
    bin/layout/TobBar.qml \
    bin/layout/PageBackground.qml


unix:!macx: LIBS += -L$$PWD/../../../../RaspiQt/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/arm-linux-gnueabihf/libc/usr/local/lib/ -lrf24

INCLUDEPATH += $$PWD/../../../../RaspiQt/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/arm-linux-gnueabihf/libc/usr/local/include/RF24
DEPENDPATH += $$PWD/../../../../RaspiQt/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/arm-linux-gnueabihf/libc/usr/local/include/RF24
