#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQuickStyle>
#include <QQuickView>
#include <QQuickWindow>

#include "bin/C++/radiothreadmenager.h"
#include "bin/C++/devicecontroller.h"
#include "bin/C++/Data_Kontainers/temphumsensordatakontainer.h"
#include "bin/C++/DeviceControllers/devicecontrollerinterface.h"
#include "bin/C++/DeviceControllers/devicefactory.h"

int main(int argc, char* argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
    qputenv("QT_QUICK_CONTROLS_STYLE", "material");
    qputenv("QT_QPA_EGLFS_PHYSICAL_WIDTH", "165");
    qputenv("QT_QPA_EGLFS_PHYSICAL_HEIGHT", "107");

    qRegisterMetaType<TempHumSensorDataKontainer>("SensorDataKontainer");

    RadioThreadMenager* radioMenager = new RadioThreadMenager;

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QQuickStyle::setStyle("Material");

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    DeviceFactory<DeviceControllerInterface> deviceFactory(engine,*radioMenager);
    deviceFactory.createDevice<DeviceController>(deviceTypes_e::TEMPERATUREDEVICE);
    DeviceControllerInterface* device = deviceFactory.getDevice(deviceTypes_e::TEMPERATUREDEVICE);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    Q_UNUSED(device);
    return app.exec();
}
