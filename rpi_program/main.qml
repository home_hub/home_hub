import QtQuick 2.11
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Universal 2.0
import QtQuick.Window 2.10
import QtQuick.VirtualKeyboard 2.1
import QtQuick.Controls.Material 2.3

import "./bin/layout/"
import "./"

ApplicationWindow {
    visible: true
    id: mainWindow

    width: 800
    height: 480

    Material.theme: Material.Dark
    Material.accent: Material.DeepOrange

    TobBar {
        id: topBar
        x: 0
        y: 0
    }
         PageBackground {
           anchors.top: topBar.bottom
           id: pageBackground

            GridView {
                id: mainWiew
                property string timeMeasure: clock()
                property var timeFunction: clock()

                anchors.fill: parent
                anchors.centerIn: parent
                cellHeight: 130
                cellWidth: 260

                model: devices
                delegate: DeviceComponent{
                    measureClk.text: mainWiew.timeMeasure
                }

//                onDataChanged: {
//                    DeviceComponent.measureClk.text = mainWiew.timeFunction
//                }
            }

//            Connections {
//                target: devices
//                onDataChanged: {
//                    DeviceComponent.measureClk.text = mainWiew.timeFunction
//                }
//            }
    }
         function clock()
         {
             var clock = new Date()
             var utc = clock.getTime() + (clock.getTimezoneOffset() * 60000)
             var nd = new Date(utc + (3600000 * 1))
             return Qt.formatTime(nd, "hh:mm:ss")
         }
}

