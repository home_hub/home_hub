import QtQuick 2.0
import QtQml 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.3
import Qt.labs.calendar 1.0
import QtQuick.Controls.Universal 2.0
import QtQuick.Controls 2.3

Rectangle {
    id: rectangle3

    Material.theme: Material.Dark
    Material.accent: Material.DeepOrange

    width: 800
    height: 480
    color: "#191919"
    clip: true
    visible: true

    Rectangle {
        id: rectangle
        x: 639
        y: 404
        width: 308
        height: 152
        color: "#3a3a3a"
        clip: false
        rotation: -43
    }

    Rectangle {
        id: rectangle1
        x: -147
        y: 404
        width: 308
        height: 152
        color: "#3a3a3a"
        rotation: 43
        clip: false
    }


}
