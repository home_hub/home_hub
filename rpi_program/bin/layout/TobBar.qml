import QtQuick 2.4

import "./"

PageBackground {
    width: 800
    height: 50
    border.width: 1
    border.color: "#3a3a3a"
    z: 0

    property alias clock: clock

    id: topBar
    Text {
        id: clock
        text: "Clock"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.top: parent.verticalCenter
        color: "White"
        font.pointSize: 12
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter

        function set() {
            var clock = new Date()
            var utc = clock.getTime() + (clock.getTimezoneOffset() * 60000)
            var nd = new Date(utc + (3600000 * 1))
            text = Qt.formatTime(nd, "hh:mm:ss")
        }

        Timer {
            id: clockTimer
            interval: 1000
            repeat: true
            running: true
            triggeredOnStart: true
            onTriggered: clock.set()
        }
    }


}
