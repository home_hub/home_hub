import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3

Sidebar {
    id: sidebar
    property int margin : 25

    ListView {

        id: options_list
        snapMode: ListView.SnapOneItem

        anchors.top: parent.clock.bottom
        anchors.bottom: parent.options_button.top

        implicitWidth: 125
        implicitHeight: 350

        orientation: ListView.VerticalTopToBottom

        clip: true
        focus: true

        delegate: ItemDelegate {
            id: itemDelegate

            width: parent.width
            height: 70

            Image{
                source: model.img_source
                anchors.horizontalCenter: parent.horizontalCenter

                height: 70
                width: 70

                MouseArea {
                    anchors.fill: parent

                    onClicked: {
                        right_view.replace(model.source)
                        console.log("clicked: + ", index)
                    }
                }
            }
        }

        model: ListModel {
            ListElement{
                source: "qrc:/bin/layout/WifiConfigSite.qml"
                img_source: "qrc:/images/wifi"
            }
        }
    }
}
