import QtQml 2.0
import QtQuick 2.4
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.4
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Imagine 2.3
import QtQuick.Controls.Material 2.3
import QtQuick.Controls.Universal 2.0

PageBackground {
    visible: true
    //backgroundImg.visible: false

    width: 125
    height: 480

    property alias clock: clock
    property alias options_button: options_button

    Text {
        id: clock
        text: "Clock"
        color: "White"
        font.pointSize: 12
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter


    }

    Image {
        id: options_button
        source: "qrc:/images/button-options.png"
        height: 70
        width: 70

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20

        MouseArea {
            anchors.fill: parent
            onClicked:{
                main_view.visible = !main_view.visible
                options_view.visible = !options_view.visible
            }
        }
    }
}

