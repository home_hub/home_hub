import QtQuick 2.11
import QtQuick.Controls.Material 2.3
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import com.my.networkModel 1.0

PageBackground{

    property alias wifi_refresh: wifi_refresh

    visible: true

    backgroundImg.visible: false

    Rectangle {
        color: parent.color
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0

        visible: true
        anchors.fill: parent

        Frame{
            x: 122
            y: 67

            ListView {
                implicitHeight: 291
                implicitWidth: 397

                id: listView
                anchors.rightMargin: 0
                anchors.leftMargin: 0
                anchors.fill: parent

                clip: true
                keyNavigationWraps: true
                spacing: 5

                model: NetworkModel {
                    list: wifiList
                }

                delegate: RowLayout{

                    Text{
                        text: index +1
                    }
                    Text {
                        text: model.Name
                    }
                    Text {
                        text: model.Ssid
                    }
                }
            }
        }

        Button {
            objectName: "refreshButton"
            id: wifi_refresh

            x: 122
            y: 388
            width: 109
            height: 39
            text: qsTr("Refresh")

            onClicked:{
                refreshWiFiList(wifiList)
            }
        }

        Button {
            id: wifi_connect
            x: 278
            y: 388
            width: 109
            height: 39
            text: qsTr("Connect")
        }

        Button {
            id: wifi_disconnect
            x: 434
            y: 388
            width: 109
            height: 39
            text: qsTr("Disconnect")
        }

        Text {
            id: element
            color: "White"
            x: 241
            y: 8
            width: 159
            height: 23
            text: qsTr("WIFi Settings")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 24
        }

    }
}

































/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
