import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Universal 2.0
import QtQuick.Window 2.10
import QtQuick.VirtualKeyboard 2.1
import QtQuick.Controls.Material 2.3

import "./"
import QtGraphicalEffects 1.0

Item{
    id: deviceComponent
    property alias measureClk: measureClk

    antialiasing: true
    z: 100

    Rectangle{

        property string textColor: "#FFFFFF"

        id: device
        height: 120
        width: 250
        radius: 40

        Material.theme: Material.Dark
        Material.accent: Material.DeepOrange

        border.width: 0

        gradient: Gradient {
            GradientStop {
                position: 0.239
                color: "#3a3a3a"

            }

            GradientStop {
                position: 0.49
                color: Material.color(Material.Grey)
            }

            GradientStop {
                position: 0.834
                color: "#3a3a3a"
            }

        }

        opacity: 0.7
        antialiasing: true
        z: 0

        Label {
            id: name
            color: parent.textColor
            x: 69
            y: 13
            font.bold: true
            text: {
                return model.name? model.name : "Pipe num: " + model.pipeNumber
            }

            horizontalAlignment: Text.AlignHCenter
            width: 112
            height: 25
            font.pixelSize: 14
        }

        TextField{
            parent: name
            x: 69
            y: 13
            width: name.width
            height: name.height

            id: nameTextField
            visible: false

            placeholderText: "Input new name"

        }

        Text {
            id: temp
            color: parent.textColor
            x: 8
            y: 44
            text: qsTr("Temperature: ")
            font.pixelSize: 16
        }

        Text {
            id: tempindex
            color: parent.textColor
            x: 151
            y: 44
            width: 74
            height: 15
            font.pixelSize: 20
            font.bold: true
            text: model.temperature.toFixed(2)+ " C"
        }

        Text {
            id: hum
            color: parent.textColor
            x: 8
            y: 75
            text: qsTr("Humidity: ")
            font.pixelSize: 16
        }

        Text {
            id: humindex
            color: parent.textColor
            x: 151
            y: 75
            font.bold: true
            text: model.humidity.toFixed(2) + " %"
            width: 74
            height: 15
            font.pixelSize: 20
        }

        Text {
            id: measureClk
            color: parent.textColor
            x: 88
            y: 97
            width: 75
            height: 15
            text: updateTime
            font.pixelSize: 14
        }

    SequentialAnimation{
        id: borderAnimation

        PropertyAction {
            target: device; property: "border.width"; }
    }
    }
    MouseArea{
        height: name.height + 20
        width: name.width + 20

        onClicked: {
            name.visible = false
            nameTextField = true
        }
    }
}
















































/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
