#ifndef TEMANDHUMDEVICE_H
#define TEMANDHUMDEVICE_H

#include <QObject>

#include <memory>

#include "deviceinterface.h"
#include "Data_Kontainers/temphumsensordatakontainer.h"

class TempAndHumDevice : public DeviceInterface
{
    Q_GADGET
    Q_INTERFACES(DeviceInterface)
    Q_PROPERTY(float humidity READ getHumidity)
    Q_PROPERTY(float temperature READ getTemperature)
    Q_PROPERTY(int pipeNumber READ getpipeNumber)
    Q_PROPERTY(QString name READ getName WRITE setName)
    Q_PROPERTY(float isActive READ getisActive)

public:
    TempAndHumDevice();
    explicit TempAndHumDevice(const TempHumSensorDataKontainer & data);
     ~TempAndHumDevice() override;

    void updateSensorVelue(const TempHumSensorDataKontainer& data) override;
    TempHumSensorDataKontainer& kontainerData() override;

    float getHumidity() const;

    float getTemperature() const;

    QString getName() const override;

    float getisActive() const;

    int getpipeNumber() const;

signals:

public slots:
    void setName(QString name) override;

private:
    std::shared_ptr<TempHumSensorDataKontainer> m_readings;
    bool m_isActive;
    QString m_name;

};

QDebug operator<<(QDebug dbg, const TempAndHumDevice& data);

#endif // TEMANDHUMDEVICE_H
