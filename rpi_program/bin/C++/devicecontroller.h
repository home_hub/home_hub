#pragma once

#include <QObject>
#include <QAbstractItemModel>

#include "deviceinterface.h"
#include "DeviceControllers/devicecontrollerinterface.h"
#include "Data_Kontainers/temphumsensordatakontainer.h"
#include "DeviceControllers/temdevicemodel.h"
#include "DeviceControllers/builder.h"

class DeviceController :public QObject, public DeviceControllerInterface
{
    Q_INTERFACES(DeviceControllerInterface)
public:
    class Builder;

    DeviceController(const DeviceController& another,QObject *parent = nullptr);

    void handleDevices(DeviceInterface* dataDevice) override;

    ~DeviceController() override;
signals:

public slots:
    void receivedParsedData(const QVariant& data) override;

protected:
       DeviceController(QObject *parent = nullptr) = delete;
       DeviceController(TemDeviceModel &model,QObject *parent = nullptr);

private:
    QList<DeviceInterface*> m_listOfDevices;
    TemDeviceModel& m_model;
};

class DeviceController::Builder{
    public:
    Builder(){}

    Builder& setModelData(TemDeviceModel* model)
    {
        m_model = model;
        return *this;
    }

    DeviceController* build()
    {
        return new DeviceController((*m_model));
    }

    private:
    TemDeviceModel* m_model;

};
