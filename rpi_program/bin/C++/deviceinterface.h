#ifndef DEVICEINTERFACE_H
#define DEVICEINTERFACE_H

#include <QObject>

#include "Data_Kontainers/temphumsensordatakontainer.h"

class DeviceInterface
{

public:
    virtual ~DeviceInterface(){}
    virtual void updateSensorVelue(const TempHumSensorDataKontainer &data) = 0;
    virtual TempHumSensorDataKontainer& kontainerData() = 0;
    virtual QString getName() const = 0;

public slots:
    virtual void setName(QString name) = 0;
private:

};

Q_DECLARE_INTERFACE(DeviceInterface,"com.GoRo3.Plugin.DeviceInterface");

#endif // DEVICEINTERFACE_H
