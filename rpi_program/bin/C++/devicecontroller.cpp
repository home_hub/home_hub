#include <QDebug>

#include <QAbstractListModel>

#include "devicecontroller.h"
#include "deviceinterface.h"
#include "temandhumdevice.h"
#include "Data_Kontainers/temphumsensordatakontainer.h"

DeviceController::DeviceController(TemDeviceModel& model, QObject *parent) :
  QObject (parent),
  m_model(model)
{
}

DeviceController::DeviceController(const DeviceController &another, QObject *parent) :
    QObject (parent),
    m_model(another.m_model)
{

}
void DeviceController::handleDevices(DeviceInterface *dataDevice)
{
    static_assert (std::is_convertible<TempAndHumDevice*,DeviceInterface*>::value, "Error: Passed data reference must by inherited by DeviceInterface");
    int devNumber = m_model.isDevicePresent(dataDevice->kontainerData().pipe());
    if(( devNumber >= 0))
    {
        m_model.updateModelData(dataDevice,devNumber);
    }
    else
    {
        m_model.appendToModel(dataDevice);
    }
}

DeviceController::~DeviceController()
{

}

void DeviceController::receivedParsedData(const QVariant& data)
{
    if(data.canConvert<TempHumSensorDataKontainer>())
    {
       TempAndHumDevice tempdevice(data.value<TempHumSensorDataKontainer>());
       DeviceController::handleDevices(&tempdevice);
    }
    else {
        qDebug() << " Uknown Data Type ";
    }
}




