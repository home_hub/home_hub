#include <QDebug>

#include "radiothreadmenager.h"
#include "nrfradio.h"
#include "RF24.h"
#include "sensordataparser.h"

RadioThreadMenager::RadioThreadMenager(QObject *parent) : QObject(parent)
{
    m_dataParser = new SensorDataParser();

    NrfRadio::instance().moveToThread(&m_radioThread);
    connect(&m_radioThread,&QThread::finished,&NrfRadio::instance(),&QObject::deleteLater);
    connect(&NrfRadio::instance(), &NrfRadio::messageArrived, m_dataParser,&SensorDataParser::receivedMessage);
    connect(this, &RadioThreadMenager::sendMessage,&NrfRadio::instance(),&NrfRadio::sendData);
    connect(&m_radioThread, &QThread::started, &NrfRadio::instance(), &NrfRadio::configure);
    m_radioThread.start();
}

RadioThreadMenager::~RadioThreadMenager(){
    m_radioThread.quit();
    m_radioThread.wait();
}

SensorDataParser &RadioThreadMenager::getDataParser() const
{
    return *m_dataParser;
}
