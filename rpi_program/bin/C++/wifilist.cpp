#include "wifilist.h"
#include <QDebug>

WifiList::WifiList(QObject* parent)
    : QObject(parent)
{
}

QVector<wifi_network> WifiList::items() const
{
    return m_networkList;
}

void WifiList::refresh_list()
{
    emit preRefresh_list();
    m_networkList.append({ "basa", "WPA2" });
    emit postRefresh_list();
    emit preRefresh_list();
    m_networkList.append({ "osa", "WPA2" });
    emit postRefresh_list();
    emit preRefresh_list();
    m_networkList.append({ "osa", "WPA2" });
    emit postRefresh_list();
    emit preRefresh_list();
    m_networkList.append({ "osa", "WPA2" });
    emit postRefresh_list();
    emit preRefresh_list();
    m_networkList.append({ "osa", "WPA2" });
    emit postRefresh_list();
    emit preRefresh_list();
    m_networkList.append({ "osa", "WPA2" });
    emit postRefresh_list();
    emit preRefresh_list();
    m_networkList.append({ "osa", "WPA2" });
    emit postRefresh_list();
}
