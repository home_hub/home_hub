﻿#ifndef NRFRADIO_H
#define NRFRADIO_H

#include <QObject>

#include "RF24.h"

class NrfRadio : public QObject
{
    Q_OBJECT
public:
    static NrfRadio& instance();

signals:
    void messageArrived(const QByteArray&,quint8 pipenum);

public slots:
    void configure();
    void sendData(const QByteArray&);

private:
   NrfRadio();
   explicit NrfRadio(QObject *parent = nullptr);
   Q_DISABLE_COPY(NrfRadio)

   void intHandler();

   qint8 m_interuptPin;
   const uint8_t m_addresses[6][6]{"1NODE","2NODE","3NODE","4NODE","5NODE"};
   RF24*  m_radio = nullptr;
};

#endif // NRFRADIO_H
