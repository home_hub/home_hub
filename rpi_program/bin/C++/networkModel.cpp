#include <QAbstractItemModel>
#include <QDebug>

#include "networkModel.h"
#include "wifilist.h"

NetworkModel::NetworkModel(QObject* parent)
    : QAbstractListModel(parent)
    , m_list(nullptr)
{
}

int NetworkModel::rowCount(const QModelIndex& parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !m_list)
        return 0;

    return m_list->items().size();
}

QVariant NetworkModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid() || !m_list)
        return QVariant();

    const wifi_network network = m_list->items().at(index.row());

    switch (role) {
    case SSID:
        return QVariant(network.SSID);
    case SECURE:
        return QVariant(network.Secure);
    }

    return QVariant("Invalid");
}

Qt::ItemFlags NetworkModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;
    return Qt::ItemIsSelectable;
}

QHash<int, QByteArray> NetworkModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[SSID] = "Name";
    names[SECURE] = "Ssid";
    return names;
}

WifiList* NetworkModel::list() const
{
    return m_list;
}

void NetworkModel::refreshList(WifiList* list)
{

    list->refresh_list();

    emit listChenged();
}

void NetworkModel::setList(WifiList* list)
{
    beginResetModel();

    if (m_list)
        m_list->disconnect(this);
    m_list = list;

    if (m_list) {
        connect(m_list, &WifiList::preRefresh_list, this, [=]() {
            const int index = m_list->items().size();
            beginInsertRows(QModelIndex(), index, index);
        });
        connect(m_list, &WifiList::postRefresh_list, this, [=]() {
            endInsertRows();
        });
    }

    endResetModel();
}
