﻿#ifndef RADIOTHREADMENAGER_H
#define RADIOTHREADMENAGER_H

#include <QObject>
#include <QThread>

#include "nrfradio.h"
#include "sensordataparser.h"

class RadioThreadMenager : public QObject
{
    Q_OBJECT
public:
    explicit RadioThreadMenager(QObject *parent = nullptr);

    ~RadioThreadMenager();
    SensorDataParser &getDataParser() const;

signals:
    void sendMessage(const QByteArray &);

public slots:

private:
    QThread m_radioThread;
    SensorDataParser* m_dataParser = nullptr;
   // NrfRadio* m_radio = nullptr;
};

#endif // RADIOTHREADMENAGER_H
