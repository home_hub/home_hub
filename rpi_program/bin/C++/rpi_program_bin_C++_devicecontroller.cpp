#include <QDebug>
#include <QAbstractItemModel>
#include <QStandardItemModel>

#include "devicecontroller.h"
#include "deviceinterface.h"
#include "temandhumdevice.h"
#include "Data_Kontainers/temphumsensordatakontainer.h"

DeviceController::DeviceController(QObject *parent) : QAbstractListModel (parent)
{

}

int DeviceController::rowCount(const QModelIndex &parent = QModelIndex()) const
{
    Q_UNUSED(parent)
    if(parent.isValid())
        return 0;
    return m_listOfDevices.size();
}

QVariant DeviceController::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
    {
      return QVariant();
    }

    const TempAndHumDevice& item = dynamic_cast<TempAndHumDevice&>(*m_listOfDevices.at(index.row()));
    switch(role){
    case HUM_E :
    {
        return QVariant(item.getHumidity());
    }
    case TEMP_E:
    {
        return QVariant(item.getTemperature());
    }
    case PIPE_E :
    {
        return QVariant(item.getpipeNumber());
    }
    case NAME_E :
    {
        return QVariant(item.getName());
    }
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> DeviceController::roleNames() const
{
    QHash<int,QByteArray> roles {
    {TEMP_E, "temperature"},
    {HUM_E, "humidity"},
    {PIPE_E, "pipeNumber"},
    {NAME_E, "name"}};

    return roles;
}

bool DeviceController::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid())
    {
      return false;
    }

    TempAndHumDevice& item = dynamic_cast<TempAndHumDevice&>(*m_listOfDevices.at(index.row()));
    switch(role){
    case NAME_E :
    {
        item.setName(value.toString());
        break;
    }
    case HUM_E :
    {
        item.kontainerData().setHum(value.toFloat());
        break;
    }
    case TEMP_E:
    {
        item.kontainerData().setTemp(value.toFloat());
        break;
    }
    case PIPE_E :
    {
         item.kontainerData().setPipe(value.toInt());
         break;
    }
    default:
        return false;
    }

    emit dataChanged(index,index,{role});
    return true;
}

Qt::ItemFlags DeviceController::flags(const QModelIndex &index) const
{
     Q_UNUSED(index);
     return static_cast<Qt::ItemFlags>(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |  Qt::ItemIsEditable);
}

void DeviceController::receivedParsedData(const QVariant& data)
{
    if(data.canConvert<TempHumSensorDataKontainer>())
    {
       TempAndHumDevice tempdevice(data.value<TempHumSensorDataKontainer>());
       DeviceController::handleDevices(&tempdevice);
    }
    else {
        qDebug() << " Uknown Data Type ";
    }
}

void DeviceController::make_Data()
{
    TempHumSensorDataKontainer* tempData = new TempHumSensorDataKontainer(2.30,3.5,234);
    TempAndHumDevice* temp1 = new TempAndHumDevice(*tempData);
    temp1->setName("Kuchnia");
    m_listOfDevices.push_back(temp1);
    TempAndHumDevice* temp2 = new TempAndHumDevice;
    m_listOfDevices.push_back(temp2);
    temp2->setName("Sypialnia");
    TempAndHumDevice* temp3 = new TempAndHumDevice;
    temp3->setName("Salon");
    m_listOfDevices.push_back(temp3);
   TempAndHumDevice* temp4 = new TempAndHumDevice;
    m_listOfDevices.push_back(temp4);
    TempAndHumDevice* temp5 = new TempAndHumDevice;
    m_listOfDevices.push_back(temp5);
    TempAndHumDevice* temp6 = new TempAndHumDevice;
    m_listOfDevices.push_back(temp6);
   const QModelIndex idx = index(0);
   emit dataChanged(idx,idx,{PIPE_E,TEMP_E});
}



