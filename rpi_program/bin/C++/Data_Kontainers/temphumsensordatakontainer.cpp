﻿#include <QDebug>

#include "temphumsensordatakontainer.h"

TempHumSensorDataKontainer::TempHumSensorDataKontainer(QObject *parent) : QObject(parent),
    m_hum(0),
    m_temp(0),
    m_pipe(0)
{

}

TempHumSensorDataKontainer::TempHumSensorDataKontainer(float hum, float temp, int pipe, QObject *parent) :
   QObject(parent),
    m_hum(hum),
    m_temp(temp),
    m_pipe(pipe)
{

}

TempHumSensorDataKontainer::TempHumSensorDataKontainer(const TempHumSensorDataKontainer &drugi, QObject *parent) :
     QObject(parent)
{
    m_temp = drugi.m_temp;
    m_hum = drugi.m_hum;
    m_pipe = drugi.m_pipe;
}

float TempHumSensorDataKontainer::hum() const
{
    return m_hum;
}

void TempHumSensorDataKontainer::setHum(float hum)
{
    m_hum = hum;
}

float TempHumSensorDataKontainer::temp() const
{
    return m_temp;
}

void TempHumSensorDataKontainer::setTemp(float temp)
{
    m_temp = temp;
}

int TempHumSensorDataKontainer::pipe() const
{
    return m_pipe;
}

void TempHumSensorDataKontainer::setPipe(int pipe)
{
    m_pipe = pipe;
}

void TempHumSensorDataKontainer::operator=(const TempHumSensorDataKontainer &data)
{
    m_hum = data.m_hum;
    m_temp = data.m_temp;
    m_pipe = data.m_pipe;
}

void TempHumSensorDataKontainer::operator=(const TempHumSensorDataKontainer *data)
{
    m_hum = data->m_hum;
    m_temp = data->m_temp;
    m_pipe = data->m_pipe;
}

QDebug operator<<(QDebug dbg,const TempHumSensorDataKontainer &data)
{
    dbg.nospace() << "Temp:" << data.temp() << "Hum:" << data.hum() << "Pipe:" << data.pipe();
    return dbg.maybeSpace();
}
