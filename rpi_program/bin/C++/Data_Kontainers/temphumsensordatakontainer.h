#ifndef TEMPHUMSENSORDATAKONTAINER_H
#define TEMPHUMSENSORDATAKONTAINER_H

#include <QObject>
#include <QtDebug>
#include <QMetaType>

class TempHumSensorDataKontainer : public QObject
{
    Q_OBJECT
public:
    explicit TempHumSensorDataKontainer(QObject *parent = nullptr);
    TempHumSensorDataKontainer(const TempHumSensorDataKontainer& drugi,QObject *parent = nullptr);
    TempHumSensorDataKontainer(float hum,float temp,int pipe,QObject *parent = nullptr);

    float hum() const;
    void setHum(float hum);

    float temp() const;
    void setTemp(float temp);

    int pipe() const;
    void setPipe(int pipe);

    void operator=(const TempHumSensorDataKontainer& data);
    void operator=(const TempHumSensorDataKontainer* data);

private:
    float m_hum;
    float m_temp;
    int m_pipe;
};

Q_DECLARE_METATYPE(TempHumSensorDataKontainer);

QDebug operator<<(QDebug dbg, const TempHumSensorDataKontainer& data);

#endif // TEMPHUMSENSORDATAKONTAINER_H
