#include "temandhumdevice.h"

#include "Data_Kontainers/temphumsensordatakontainer.h"

TempAndHumDevice::TempAndHumDevice()
{
    m_readings = std::make_shared<TempHumSensorDataKontainer>();
}

TempAndHumDevice::TempAndHumDevice(const TempHumSensorDataKontainer &data)
{
    m_readings = std::make_shared<TempHumSensorDataKontainer>(data);
}

TempAndHumDevice::~TempAndHumDevice()
{

}
void TempAndHumDevice::updateSensorVelue(const TempHumSensorDataKontainer& data)
{
    m_readings = std::make_shared<TempHumSensorDataKontainer>(data);
}

TempHumSensorDataKontainer &TempAndHumDevice::kontainerData()
{
    return *m_readings;
}

float TempAndHumDevice::getHumidity() const
{
    return m_readings->hum();
}

float TempAndHumDevice::getTemperature() const
{
    return m_readings->temp();
}

QString TempAndHumDevice::getName() const
{
    return m_name;
}

float TempAndHumDevice::getisActive() const
{
    return m_isActive;
}

int TempAndHumDevice::getpipeNumber() const
{
     return m_readings->pipe();
}

void TempAndHumDevice::setName(QString name)
{
    m_name = name;
}

QDebug operator<<(QDebug dbg, const TempAndHumDevice &data)
{
    dbg.nospace() << "Temp: " << data.getTemperature() << "Hum: " << data.getHumidity() << "Pipe: " << data.getpipeNumber();

    return dbg;
}
