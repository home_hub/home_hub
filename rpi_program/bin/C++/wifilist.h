#pragma once

#include <QObject>
#include <QVector>

struct wifi_network {

    QString SSID;
    QString Secure;
};

class WifiList : public QObject {
    Q_OBJECT

public:
    explicit WifiList(QObject* parent = nullptr);

    QVector<wifi_network> items() const;
    void refresh_list();

signals:
    void preRefresh_list();
    void postRefresh_list();
    void items_refreshed();

public slots:

private:
    QVector<wifi_network> m_networkList;
};
