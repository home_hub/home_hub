﻿#ifndef SENSORDATAPARSER_H
#define SENSORDATAPARSER_H

#include <QObject>

#include "Data_Kontainers/datakonteinerinterface.h"

class SensorDataParser : public QObject
{
    Q_OBJECT
public:
    explicit SensorDataParser(QObject *parent = nullptr);

    enum ParseError_E{BAD_RESPONSE_E, BAD_DATA_ERROR_E};
    Q_ENUM(ParseError_E)

signals:
    void parseError(ParseError_E);
    void parsedMessage(const QVariant &);

public slots:
     void receivedMessage(const QByteArray&,quint8 pipenum);

private:

   QString m_dataBuffer;

   enum m_parseState{STATE1_E,STATE2_E,STATE3_E,STATE4_E,STATE_ERROR_E};

   void parseData(quint8 pipeNumber);
};

#endif // SENSORDATAPARSER_H
