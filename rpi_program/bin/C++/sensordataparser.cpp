#include <QDebug>
#include <QtConcurrent>

#include "sensordataparser.h"
#include "Data_Kontainers/temphumsensordatakontainer.h"
#include "Data_Kontainers/datakonteinerinterface.h"

SensorDataParser::SensorDataParser(QObject *parent) : QObject(parent)
{}

void SensorDataParser::receivedMessage(const QByteArray& message,quint8 pipenum)
{
    m_dataBuffer = message;

   QtConcurrent::run(this,&SensorDataParser::parseData,pipenum);
}

void SensorDataParser::parseData(quint8 pipeNumber)
{
    bool endparse = false;

    float humidity;
    float temperature;

    m_parseState state = m_parseState::STATE1_E;

    QString tempString, humString;

    while(!endparse){

        switch(state)
        {
        case STATE1_E:
        {

            if(m_dataBuffer.startsWith("AT+")){
                state=STATE2_E;
                break;
            }
        }
            //free fall
        case STATE2_E:
        {
            if(m_dataBuffer.contains("H"))
            {
                int index = m_dataBuffer.indexOf('H');
                while(m_dataBuffer[index+1]!='T')
                {
                    index+=1;
                    humString += m_dataBuffer[index];
                }
                state = STATE3_E;
                break;
            }
        }
        case STATE3_E:
        {
            if(m_dataBuffer.contains('T'))
            {
                int index = m_dataBuffer.indexOf('T',2);
                while(m_dataBuffer[index+1]!='\0'){
                        index+=1;
                        tempString += m_dataBuffer[index];
                }
                state = STATE4_E;
                break;
            }
        }

        case STATE4_E:
        {
            bool convertCompleted;

            humidity = humString.toFloat(&convertCompleted);
            if(!convertCompleted){
                emit parseError(ParseError_E::BAD_DATA_ERROR_E);
                break;
            }

            temperature = tempString.toFloat(&convertCompleted);
            if(!convertCompleted){
                emit parseError(ParseError_E::BAD_DATA_ERROR_E);
                break;
            }

// To-D0 Make some nice refactor of this code uncluding mayby new method
// for emiting parsedMessage - GoRo3

            QVariant dataParsed;
            dataParsed.setValue(TempHumSensorDataKontainer(humidity,temperature,pipeNumber));

            emit parsedMessage(dataParsed);

            endparse=true;
            break;
        }

        default:
        {
            state = STATE1_E;
            endparse = true;
            emit parseError(ParseError_E::BAD_RESPONSE_E);
            break;
        }

        }

    }
}
