#include <QDebug>
#include <QThread>

#include <memory>

#include "nrfradio.h"
#include "RF24.h"

static NrfRadio* thisPtrRadio;
struct DHT22_DATA_t
{
    qint8 temperature_integral;
    quint8 temperature_decimal;
    quint8 humidity_integral;
    quint8 humidity_decimal;
    quint8 retryCount;
};

NrfRadio::NrfRadio(QObject *parent) :
    QObject(parent)
    ,m_addresses{"1NODE","2NODE","3NODE","4NODE","5NODE"}
{
     m_radio = new RF24(25,BCM2835_SPI_CS0,BCM2835_SPI_SPEED_4MHZ);
     if(!m_radio){
         qDebug() << "Inicialization of m_radio error";
     }
     m_interuptPin = 23;

     thisPtrRadio = this;
}

NrfRadio::NrfRadio()
{
    m_radio = new RF24(25,BCM2835_SPI_CS0,BCM2835_SPI_SPEED_4MHZ);
    if(!m_radio){
        qDebug() << "Inicialization of m_radio error";
    }
    m_interuptPin = 23;

    thisPtrRadio = this;
}


void NrfRadio::intHandler()
{
  quint8 pipenum;
  quint8 payloadSize;
  QByteArray buffer;

  bool tx_ok,tx_fail,rx;
  m_radio->whatHappened(tx_ok,tx_fail,rx);
  if(rx){
      if(m_radio->available(&pipenum))
      {
        payloadSize = m_radio->getDynamicPayloadSize();
        buffer.resize(payloadSize);
        m_radio->read(buffer.data(), payloadSize);
        emit messageArrived(buffer,pipenum);
        m_radio->startListening();
        return;
        }
  }
  if(tx_ok)
  {
      qDebug() << "Message send";
      return;
  }
  if(tx_fail)
  {
      qDebug() << "Sending failed.\n\r";
      return;
  }
}

NrfRadio &NrfRadio::instance()
{
    static std::unique_ptr<NrfRadio> instance (new NrfRadio(nullptr));
    return *instance;
}

void NrfRadio::configure()
{
    m_radio->begin();

    m_radio->enableAckPayload();
    m_radio->enableDynamicPayloads();
    m_radio->setRetries(1,5);
    m_radio->setAutoAck(true);
    m_radio->setCRCLength(RF24_CRC_8);
    m_radio->setChannel(75);                  
    m_radio->setAddressWidth(5);

    m_radio->openWritingPipe(m_addresses[0]);

    m_radio->openReadingPipe(1,m_addresses[0]);
    m_radio->openReadingPipe(2,m_addresses[1]);
    m_radio->openReadingPipe(3,m_addresses[2]);
    m_radio->openReadingPipe(4,m_addresses[3]);
    m_radio->openReadingPipe(5,m_addresses[4]);

    m_radio->startListening();

    attachInterrupt(m_interuptPin,INT_EDGE_FALLING,[](){
                                                              thisPtrRadio->intHandler();
                                                                    });

    m_radio->maskIRQ(1,0,0);
    m_radio->printDetails();
}

void NrfRadio::sendData(const QByteArray &message)
{
       m_radio->stopListening();
       m_radio->writeFast(message,static_cast<uint8_t>(message.size()));
       m_radio->txStandBy();
       m_radio->startListening();
}






