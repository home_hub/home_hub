#ifndef DEVICECONTROLLERINTERFACE_H
#define DEVICECONTROLLERINTERFACE_H

#include <QObject>

#include "../deviceinterface.h"

class DeviceControllerInterface
{
public:
    virtual ~DeviceControllerInterface(){}

    virtual void handleDevices(DeviceInterface* dataDevice) = 0;
    virtual void receivedParsedData(const QVariant& data) = 0;
private:

};

Q_DECLARE_INTERFACE(DeviceControllerInterface,"com.GoRo3.Plugin.DeviceControllerInterface");

#endif // DEVICECONTROLLERINTERFACE_H
