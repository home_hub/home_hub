﻿#ifndef TEMDEVICEMODEL_H
#define TEMDEVICEMODEL_H

#include <QObject>
#include <QAbstractItemModel>
#include "../deviceinterface.h"

class TemDeviceModel : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(TemDeviceModel)
    Q_ENUMS(role_names)
public:
    enum roleNames {
      TEMP_E = Qt::UserRole +1,
      HUM_E,
      PIPE_E,
      NAME_E
    };

    explicit TemDeviceModel (QObject *parent = nullptr);
    QList<DeviceInterface *> getListOfDevices() const;
    int isDevicePresent(int pipeNumber);

    // QAbstractListModel interface
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

public slots:
    void appendToModel(DeviceInterface* device);
    void updateModelData(DeviceInterface* device, int position);
private:
    QList<DeviceInterface*> m_listOfDevices;

};

#endif // TEMDEVICEMODEL_H
