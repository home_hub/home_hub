#pragma once
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QString>
#include <QMap>

#include "../devicecontroller.h"
#include "../radiothreadmenager.h"
enum class deviceTypes_e{
     TEMPERATUREDEVICE = 1,
     BUTTONDEVICE
};

template<typename TBase>
class DeviceFactory
{
public:
    explicit DeviceFactory(QQmlApplicationEngine& engine, RadioThreadMenager& radioThread):
        m_engine(engine),
        m_radioThread(radioThread)
    {}

    template<typename TDerived>
    void createDevice(deviceTypes_e type)
    {
        static_assert (std::is_base_of<TBase,TDerived>::value,"Factory::registerType doesn't accept this type because doesn't derive from base class");

        switch(type)
        {
        case deviceTypes_e::TEMPERATUREDEVICE:
        {
            m_createtFunctions[type] = createTemperatureDevice();
            break;
        }
        default:
        {
            throw std::invalid_argument("Not know device type");
        }

        }
    }

    TBase* getDevice(deviceTypes_e type) const
    {
        typename QMap<deviceTypes_e,TBase*>::const_iterator it = m_createtFunctions.find(type);
        if((it != m_createtFunctions.end()))
        {
            return it.value();
        }
        return nullptr;
    }
private:
    typedef TBase* (*CallbacFunc)();

    TBase* createTemperatureDevice()
    {
        TemDeviceModel* model = new TemDeviceModel;
        DeviceController* t = DeviceController::Builder().setModelData(model).build();

        QObject::connect(&m_radioThread.getDataParser(),&SensorDataParser::parsedMessage,t,&DeviceController::receivedParsedData, Qt::QueuedConnection);
        m_engine.rootContext()->setContextProperty(QStringLiteral("devices"),model);
        return t;
    }

    QMap<deviceTypes_e, TBase*> m_createtFunctions;
    QQmlApplicationEngine& m_engine;
    RadioThreadMenager& m_radioThread;
};



