#include "temdevicemodel.h"
#include "../temandhumdevice.h"

TemDeviceModel::TemDeviceModel(QObject *parent) : QAbstractListModel (parent)
{
}

int TemDeviceModel::rowCount(const QModelIndex &parent = QModelIndex()) const
{
    Q_UNUSED(parent)
    if(parent.isValid())
        return 0;
    return m_listOfDevices.size();
}

QVariant TemDeviceModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
    {
        return QVariant();
    }

    const TempAndHumDevice& item = dynamic_cast<TempAndHumDevice&>(*m_listOfDevices.at(index.row()));
    switch(role){
    case HUM_E :
    {
        return QVariant(item.getHumidity());
    }
    case TEMP_E:
    {
        return QVariant(item.getTemperature());
    }
    case PIPE_E :
    {
        return QVariant(item.getpipeNumber());
    }
    case NAME_E :
    {
        return QVariant(item.getName());
    }
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> TemDeviceModel::roleNames() const
{
    QHash<int,QByteArray> roles {
        {TEMP_E, "temperature"},
        {HUM_E, "humidity"},
        {PIPE_E, "pipeNumber"},
        {NAME_E, "name"}};

    return roles;
}

bool TemDeviceModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid())
    {
        return false;
    }

    TempAndHumDevice& item = dynamic_cast<TempAndHumDevice&>(*m_listOfDevices.at(index.row()));
    switch(role){
    case NAME_E :
    {
        item.setName(value.toString());
        break;
    }
    case HUM_E :
    {
        item.kontainerData().setHum(value.toFloat());
        break;
    }
    case TEMP_E:
    {
        item.kontainerData().setTemp(value.toFloat());
        break;
    }
    case PIPE_E :
    {
         item.kontainerData().setPipe(value.toInt());
         break;
    }
    default:
        return false;
    }

    emit dataChanged(index,index,{role});
    return true;
}

Qt::ItemFlags TemDeviceModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return static_cast<Qt::ItemFlags>(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |  Qt::ItemIsEditable);
}

void TemDeviceModel::appendToModel(DeviceInterface *device)
{
    int listLenght = m_listOfDevices.length();
    beginInsertRows(QModelIndex(),listLenght,listLenght);
    TempAndHumDevice* dataDevice = new TempAndHumDevice(dynamic_cast<TempAndHumDevice&>(*device));
    m_listOfDevices.push_back(dataDevice);
    endInsertRows();
}

void TemDeviceModel::updateModelData(DeviceInterface* device, int position)
{
    m_listOfDevices.at(position)->kontainerData().setHum(device->kontainerData().hum());
    m_listOfDevices.at(position)->kontainerData().setTemp(device->kontainerData().temp());
    const QModelIndex idx = index(position,0);
    emit dataChanged(idx,idx,{HUM_E,TEMP_E});
}

QList<DeviceInterface *> TemDeviceModel::getListOfDevices() const
{
    return m_listOfDevices;
}

int TemDeviceModel::isDevicePresent(int pipeNumber)
{
    int i = 0;
    if(!(m_listOfDevices.empty()))
    {
        for(DeviceInterface* device: m_listOfDevices)
        {
            if(TempAndHumDevice* tempDevice = dynamic_cast<TempAndHumDevice*>(device))
            {
                if(pipeNumber == tempDevice->getpipeNumber())
                {
                    return i;
                }
            }
            i++;
        }
        return -1;
    }
    else
    {
        return -1;
    }

}
