#ifndef WIFI_MENAGER_H
#define WIFI_MENAGER_H

#include <QAbstractListModel>

#include "wifilist.h"

class NetworkModel : public QAbstractListModel {

    Q_OBJECT
    Q_PROPERTY(WifiList* list READ list WRITE setList NOTIFY listChenged)

public:
    explicit NetworkModel(QObject* parent = nullptr);

    enum {
        SSID = Qt::UserRole,
        SECURE
    };

    int
    rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;
    virtual QHash<int, QByteArray> roleNames() const override;

    void setList(WifiList* list);
    WifiList* list() const;

signals:
    void listChenged();

public slots:
    void refreshList(WifiList* list);

private:
    WifiList* m_list;
};

#endif // WIFI_MENAGER_H
